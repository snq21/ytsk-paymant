<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientPaymants;

/**
 * ClientPaymantsSearch represents the model behind the search form of `app\models\ClientPaymants`.
 */
class ClientPaymantsSearch extends ClientPaymants
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id', 'Client_id'], 'integer'],
            [['Calculation_period', 'Type_of_payment', 'Calculation_type', 'Unit_of_measurement'], 'safe'],
            [['Tariff', 'Normative', 'Quantity', 'Accrued', 'Allocation', 'Paid', 'Balance_at_the_beginning'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientPaymants::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'Client_id' => \Yii::$app->user->getId(),

        ]);

        $query->andFilterWhere(['like', 'Calculation_period', $this->Calculation_period])
            ->andFilterWhere(['like', 'Type_of_payment', $this->Type_of_payment])
            ->andFilterWhere(['like', 'Calculation_type', $this->Calculation_type])
            ->andFilterWhere(['like', 'Unit_of_measurement', $this->Unit_of_measurement]);

        return $dataProvider;
    }
}
