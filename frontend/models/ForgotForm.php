<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ForgotForm extends Model
{
    public $login;
    public $house;
    public $apartment;
    public $password;
    private $_user;
    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['login', 'house','apartment','verifyCode'], 'required'],
            // rememberMe must be a boolean value

            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Новый Пароль',
            'login' => 'Лицевой счет',
            'house' => 'Дом',
            'apartment' => 'Квартира',
            'verifyCode'=>'Капча'
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->find();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный лицевой счет или пароль.');
            }
        }
    }


    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function find_client()
    {
        return Clients::findOne(['Id'=>$this->login,'House'=>$this->house,'Apartment'=>$this->apartment]);
    }

}
