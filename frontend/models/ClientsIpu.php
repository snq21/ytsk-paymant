<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Clients_ipu".
 *
 * @property int $id
 * @property int|null $Client_id
 * @property int|null $IPU_code
 * @property string|null $Type_of_control
 * @property string|null $Brand
 * @property string|null $Model
 * @property string|null $Serial_number
 * @property string|null $Installation_date
 * @property string|null $Date_of_verification
 * @property string|null $Installation_location
 * @property float|null $Initial_indications
 * @property float|null $Final_indications
 * @property string $Final_indications_history
 * @property string $Initial_indications_history
 */
class ClientsIpu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients_ipu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Client_id', 'IPU_code'], 'integer'],
            [['Initial_indications', 'Final_indications'], 'number'],
            [['Final_indications_history', 'Initial_indications_history'], 'required'],
            [['Final_indications_history', 'Initial_indications_history'], 'string'],
            [['Type_of_control', 'Brand', 'Model', 'Serial_number', 'Installation_location'], 'string', 'max' => 255],
            [['Installation_date', 'Date_of_verification'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Client_id' => 'Лицевой счет',
            'IPU_code' => 'Код ИПУ',
            'Type_of_control' => 'Вид КУ',
            'Brand' => 'Марка',
            'Model' => 'Модель',
            'Serial_number' => 'Заводской номер',
            'Installation_date' => 'Дата установки',
            'Date_of_verification' => 'Дата поверки',
            'Installation_location' => 'Место установки',
            'Initial_indications' => 'Начальные показания',
            'Final_indications' => 'Конечные показания',
            'Final_indications_history' => 'Конечные показания история',
            'Initial_indications_history' => 'Начальные показания история',
        ];
    }
}
