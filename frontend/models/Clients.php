<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "Clients".
 *
 * @property int $Id
 * @property string|null $Name
 * @property string|null $City
 * @property string|null $Street
 * @property string|null $House
 * @property string|null $Building
 * @property string|null $Apartment
 * @property string|null $Room
 * @property string|null $ELS
 * @property float|null $Area
 * @property float|null $House_area
 * @property float|null $MOS_area
 * @property int|null $Number_of_people
 * @property string|null $password
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $authKey
 * @property string|null $accessToken
 */
class Clients extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id'], 'required'],
            [['Id', 'Number_of_people'], 'integer'],
            [['Area', 'House_area', 'MOS_area'], 'number'],
            [['Name', 'City', 'Street','email','phone'], 'string', 'max' => 255],
            [['House', 'Building', 'Apartment', 'Room'], 'string', 'max' => 10],
            [['ELS'], 'string', 'max' => 200],
            [['Id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Name' => 'Фио',
            'City' => 'Город',
            'Street' => 'Улица',
            'House' => 'Дом',
            'Building' => 'Корпус',
            'Apartment' => 'Квартира',
            'Room' => 'Комната',
            'ELS' => 'ЕЛС',
            'Area' => 'Площадь',
            'House_area' => 'Площадь дома',
            'MOS_area' => 'Площадь МОП',
            'Number_of_people' => 'Количество человек',
            'password'=> 'Пароль',
            'email'=> 'Почта',
            'phone'=> 'Телефон',
        ];
    }

    /**
     * @inheritDoc
     */
    public static function findIdentity($id)
    {
        return Clients::findOne($id);

    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return Clients::findOne(['accessToken'=>$token]);
    }
    public function validatePassword($password)
    {
        return $this->password==sha1($password);
    }



    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritDoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey==$authKey;
    }
}
