<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Восстановить пароль';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->session->hasFlash('forgoted')): ?>

        <div class="alert alert-success">
            Если вы правильно ввели свои данные вы можете войти в систему использую введенный пароль
        </div>

    <?php else: ?>
        <div class="msg-form">
            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'login')->textInput(['maxlength' => 6]) ?>
            <?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'apartment')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'verifyCode')->widget(\yii\captcha\Captcha::className(), [
                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
            ]) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Восстановить пароль'), ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    <?php endif; ?>
</div>

