<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Профиль';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                        Общая информация
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pay-list-tab" data-toggle="tab" href="#pay-list" role="tab" aria-controls="pay-list" aria-selected="false">Детализация счета</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tariffs-tab" data-toggle="tab" href="#tariffs" role="tab" aria-controls="tariffs" aria-selected="false">Тарифы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="counters-tab" data-toggle="tab" href="#counters" role="tab" aria-controls="counters" aria-selected="false">Счётчики</a>
                </li>

            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class="card pt-2">
                        <div class="card-header">
                            О клиенте
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Данные клиента</h5>

                            <p class="card-text">
                                Лицевой счёт: <?=$modelClient->Id?><br>
                                ФИО: <?=$modelClient->Name?><br>
                                Адрес: г. <?=$modelClient->City?>,<?=$modelClient->Street?>, д. <?=$modelClient->House?> <?=trim($modelClient->Building)!=null ? ' корпус '.$modelClient->Building: '' ?> кв. <?=$modelClient->Apartment?> <?=trim($modelClient->Room)!=null ? ' комната '.$modelClient->Room: '' ?>
                            </p>

                            <a href="<?=\yii\helpers\Url::to(['/clients/view'])?>" class="btn btn-primary">Подробнее</a>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="tariffs" role="tabpanel" aria-labelledby="tariffs-tab">
                    <div class="card pt-2">
                        <div class="card-header">
                            Тарифы
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Тарифы</h5>
                            <?php
                            $result='';
                            $currentType='';
                            foreach ($modelPayment as $key=>$item) {

                                if($item['Type_of_payment']!=$currentType){
                                    $result.='<tr>';
                                        $result.='<td colspan="9">';
                                            $result.="Вид платежа ".$item['Type_of_payment'];
                                        $result.='</td>';
                                    $result.='</tr>';
                                }
                                $result.='<tr>';
                                    $result.='<td>';
                                        $result.=$item['Id'];
                                    $result.='</td>';
                                    $result.='<td>';
                                        $result.=$item['Calculation_period'];
                                    $result.='</td>';
                                $result.='<td>';
                                $result.=$item['Calculation_type'];
                                $result.='</td>';

                                $result.='<td>';
                                        $result.=$item['Type_of_payment'];
                                    $result.='</td>';
                                $result.='<td>';
                                    $result.=$item['Normative'];
                                $result.='</td>';
                                $result.='<td>';
                                    $result.=$item['Unit_of_measurement']." (".$item['Calculation_type'].")";
                                $result.='</td>';

                                $result.='<td>';
                                    $result.=$item['Tariff'];
                                $result.='</td>';

                                $result.='</tr>';
                                $currentType=$item['Type_of_payment'];

                            }
                            ?>
                            <table class="table table-bordered">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Период
                                    </th>
                                    <th>
                                    Услуга
                                    </th>
                                    <th>
                                        Вид платежа
                                    </th>
                                    
                                    <th>
                                        Норматив
                                    </th>
                                    <th>
                                        Ед. изм.
                                    </th>
                                    <th>Тариф, руб. за ед. изм.</th>

                                </tr>
                                <?=$result?>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="tab-pane fade" id="pay-list" role="tabpanel" aria-labelledby="pay-list-tab">
                    <div class="card pt-2">
                        <div class="card-header">
                            Детализация счета
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Детализация счета</h5>
                            <?php
                            $result='';
                            $summ=[];
                            $curentCalcType='';
                            foreach ($modelPayment as $key=>$item) {

                                if($item['Type_of_payment']!=$currentType){
                                    $result.='<tr>';
                                    $result.='<td colspan="9">';
                                    $result.="Вид платежа ".$item['Type_of_payment'];
                                    $result.='</td>';
                                    $result.='</tr>';
                                }

                                $result.='<tr>';
                                $result.='<td>';
                                $result.=$item['Id'];
                                $result.='</td>';
                                $result.='<td>';
                                $result.=$item['Calculation_period'];
                                $result.='</td>';

                                $result .= '<td>';
                                $result .= $item['Calculation_type'];
                                $result .= '</td>';

                                $result.='<td>';
                                $result.=$item['Type_of_payment'];
                                $result.='</td>';
                                $result.='<td>';
                                $result.=$item['Balance_at_the_beginning'];
                                $result.='</td>';
                                $result.='<td>';
                                $result.=$item['Accrued'];
                                $result.='</td>';
                                $result.='<td>';
                                $result.=$item['Allocation'];
                                $result.='</td>';
                                $result.='<td>';
                                $result.=$item['Paid'];
                                $result.='</td>';
                                
                                $result.='<td>';
                                $result.=(($item['Balance_at_the_beginning']+$item['Accrued'])-$item['Paid']);
                                $result.='</td>';
                                $result.='</tr>';
                                $curentCalcType=$item['Calculation_type'];

                            }
                            ?>
                            <table class="table table-bordered">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Период
                                    </th>
                                    <th>
                                        Услуга  
                                    </th>
                                    <th>
                                        Вид платежа
                                    </th>
                                    
                                    <th>
                                        Остаток на начало месяца, руб.
                                    </th>
                                    <th>
                                        Начисления, руб.
                                    </th>
                                    <th>
                                        перерасчёты, руб.
                                    </th>
                                    <th>
                                        Оплачено, руб.
                                    </th>
                                    <th>
                                        Остаток на конец месяца, руб.
                                    </th>

                                </tr>
                                <?=$result?>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="counters" role="tabpanel" aria-labelledby="counters-tab">
                    <div class="card pt-2">
                        <div class="card-header">
                            Счётчики
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Списико счётчиков</h5>
                            <?php
                            $result='';
                            foreach ($modelIpu as $key=>$item) {

                                $result .= '<tr>';
                                $result .= '<td>';
                                    $result .= $item['id'];
                                $result .= '</td>';
                                $result .= '<td>';
                                    $result .= $item['IPU_code'];
                                $result .= '</td>';
                                $result .= '<td>';
                                    $result .= $item['Calculation_type'];
                                $result .= '</td>';
                                $result .= '<td>';
                                    $result .= $item['Type_of_control'];
                                $result .= '</td>';
                                
                                $result .= '<td>';
                                    $result .= $item['Installation_date'];
                                $result .= '</td>';
                                $result .= '<td>';
                                    $result .= $item['Date_of_verification'];
                                $result .= '</td>';
                                $result .= '<td>';
                                    $result .= $item['Initial_indications'];
                                $result .= '</td>';
                                $result .= '<td>';
                                    $result .= $item['Final_indications'];
                                $result .= '</td>';
                                $result .= '<td>';
                                    $result .= "<a href='".\yii\helpers\Url::to(['client-ipu/update','id'=>$item['id']])."' class='btn-danger btn btn-sm'>Добавить показания</a>";
                                $result .= '</td>';
                                $result .= '</tr>';

                            }
                            ?>
                            <table class="table table-bordered">
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Код ИПУ
                                    </th>
                                    <th>
                                        Вид КУ
                                    </th>
                                    <th>
                                        Дата установки
                                    </th>
                                    <th>
                                        Дата проверки
                                    </th>
                                    <th>Начальные показания</th>
                                    <th>
                                        Конечные показания
                                    </th>
                                    <th>
                                        Действия
                                    </th>
                                </tr>
                                <?=$result?>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

</div>
