<?php

/* @var $this yii\web\View */

$this->title = 'ЮТСК';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Личный кабинет ЮТСК</h1>

        <p class="lead">Добро пожаловать.</p>

        <p><a class="btn btn-lg btn-success" href="<?=\yii\helpers\Url::toRoute(['site/login'])?>">Войти</a></p>
    </div>
</div>