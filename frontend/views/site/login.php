<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">


    <div class="card ">
        <div class="card-header">
            <p>Вход</p>
        </div>
        <div class="card-body">
            <h5 class="card-title">Введите данные для авторизации:</h5>
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'login')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '999999',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => ('Лицевой счет')
                ],

            ])
            ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} Запомнить меня</div>\n<div class=\"col-lg-8\">{error}</div>",
            ]) ?>

            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <a href="<?=\yii\helpers\Url::toRoute(['site/forgot'])?>" class="btn btn-link">Забыли пароль?</a>
                    <?= Html::submitButton('Вход', ['class' => 'btn btn-danger', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>




</div>
