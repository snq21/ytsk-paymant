<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;


use app\assets\AppAsset;



$items=null;


if (!empty(Yii::$app))
{

            if (Yii::$app->user->isGuest){

                $items = [
                    ['label' => 'Домой', 'url' => ['/site/index']],
                    ['label' => 'О нас', 'url' => ['/site/about']],
                    ['label' => 'Связаться с нами', 'url' => ['/site/contact']],
                    ['label' => 'Вход', 'url' => ['/site/login']],

                ];
            }else{

                $items=[
                    ['label' => 'Домой', 'url' => ['/site/profile']],
                    ['label' => 'ИПУ', 'url' => ['/client-ipu/index']],
                    ['label' => 'Платежи', 'url' => ['/client-paymants/index']],
                    ['label' => 'О нас', 'url' => ['/site/about']],
                    ['label' => 'Связаться с нами', 'url' => ['/site/contact']],
                    ['label' => 'Личный кабинет', 'url' => ['/site/profile']],
                    ['label' => 'Заявки', 'url' => ['/msg/index']],
                    ['label' => 'Выход', 'url' => ['/site/logout']]
                ];
            }
}

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php

    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
    ]);
echo Nav::widget([
    'items' => $items,
    'options' => ['class' => 'navbar-nav navbar-expand-lg navbar-light ml-auto bg-light'],
]);
NavBar::end();

    ?>

    <div class="container-fluid">
        <div class="row pt-2">
            <div class="col-md-12">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
            </div>
        </div>

        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container-fluid">
        <p class="pull-left">&copy; ЮТСК <?= date('Y') ?></p>


    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
