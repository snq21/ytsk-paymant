<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientPaymantsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Платежи');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col">

        <h1><?= Html::encode($this->title) ?></h1><?php Pjax::begin(); ?>
        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => [ 'style' => 'table-layout:fixed;' ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn',
                ],

                'Calculation_period',

                [
                    'attribute' => 'Type_of_payment',
                    'label' => 'Тип платежа',
                    'value' => function($model) { return $model->Type_of_payment  . " " . $model->Calculation_type. " " .
                        $model->Unit_of_measurement;},
                ],


                'Tariff',
                'Normative',
                'Quantity',
                'Accrued',
                'Allocation',
                'Paid',
                'Balance_at_the_beginning',
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="btn btn-danger">Посмотреть</span>', $url, [
                                'title' => 'View',
                            ]);
                        }]
                ],

            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

</div>
