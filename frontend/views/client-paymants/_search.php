<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientPaymantsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-paymants-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>



    <?= $form->field($model, 'Calculation_period')->dropDownList(
        \yii\helpers\ArrayHelper::map(
            \app\models\ClientPaymants::find()->select(['Calculation_period'])->orderBy('Calculation_period')->groupBy('Calculation_period')->all()

                , 'Calculation_period', 'Calculation_period')
    ) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Найти'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Сброс'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
