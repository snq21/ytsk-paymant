<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientPaymants */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-paymants-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Calculation_period')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Type_of_payment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Calculation_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Unit_of_measurement')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tariff')->textInput() ?>

    <?= $form->field($model, 'Normative')->textInput() ?>

    <?= $form->field($model, 'Quantity')->textInput() ?>

    <?= $form->field($model, 'Accrued')->textInput() ?>

    <?= $form->field($model, 'Allocation')->textInput() ?>

    <?= $form->field($model, 'Paid')->textInput() ?>

    <?= $form->field($model, 'Balance_at_the_beginning')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
