<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientPaymants */

$this->title = Yii::t('app', 'Create Client Paymants');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Client Paymants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-paymants-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
