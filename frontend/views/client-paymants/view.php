<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientPaymants */

$this->title =$model->Calculation_period;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Платежи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="client-paymants-view">
    <a class="btn btn-danger" href="https://yu-tsk74.ru/new/project_frontend/web/<?=$fname?>">Скачать платежную квитанцию</a>
    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'Client_id',
            'Calculation_period',
            'Type_of_payment',
            'Calculation_type',
            'Unit_of_measurement',
            'Tariff',
            'Normative',
            'Quantity',
            'Accrued',
            'Allocation',
            'Paid',
            'Balance_at_the_beginning',
        ],
    ]) ?>

</div>
