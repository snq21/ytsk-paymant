<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsIpu */

$this->title = Yii::t('app', 'Добавить показания к счетчику: {name}', [
    'name' => $model->IPU_code,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ИПУ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Добавить');
?>
<div class="clients-ipu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
