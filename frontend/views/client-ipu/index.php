<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientsIpuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'ИПУ');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-ipu-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'IPU_code',
            'Type_of_control',
            'Installation_date',
            'Date_of_verification',
            'Initial_indications',
            'Final_indications',
            [
                'format' => 'html',
                'attribute' => 'Initial_indications_history',
                'value' => function($model){
                    $tr='<table class="table table-bordered">';
                    $tr.='<tr><th>Месяц</th><th>Значения</th></tr>';
                    foreach (json_decode($model->Initial_indications_history) as $key=> $item){
                        $tr.='<tr>';
                        $tr.='<td>'.($item->dt).'</td>';
                        $tr.='<td>'.($item->val).'</td>';
                        $tr.='</tr>';
                    }
                    $tr.='</table>';
                    return  $tr;
                }
            ],[
                'format' => 'html',
                'attribute' => 'Final_indications_history',
                'value' => function($model){
                    $tr='<table class="table table-bordered">';
                    $tr.='<tr><th>Месяц</th><th>Значения</th></tr>';
                    foreach (json_decode($model->Final_indications_history) as $key=> $item){
                        $tr.='<tr>';
                        $tr.='<td>'.($item->dt).'</td>';
                        $tr.='<td>'.($item->val).'</td>';
                        $tr.='</tr>';
                    }
                    $tr.='</table>';
                    return  $tr;
                }
            ],


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="btn btn-danger btn-sm">Посмотреть</span><br>', $url, [
                            'title' => 'View',
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="btn btn-danger btn-sm">Обновить</span>', $url, [
                            'title' => 'View',
                        ]);
                    }
                    ]
            
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
