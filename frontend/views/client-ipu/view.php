<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsIpu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ИПУ'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clients-ipu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'Client_id',
            'IPU_code',
            'Type_of_control',
            'Brand',
            'Model',
            'Serial_number',
            'Installation_date',
            'Date_of_verification',
            'Installation_location',
            'Initial_indications',
            'Final_indications',
            [
                'format' => 'html',
                'attribute' => 'Initial_indications_history',
                'value' => function($model){
                    $tr='<table class="table table-bordered">';
                    $tr.='<tr><th>Месяц</th><th>Значения</th></tr>';
                    foreach (json_decode($model->Initial_indications_history) as $key=> $item){
                        $tr.='<tr>';
                        $tr.='<td>'.($item->dt).'</td>';
                        $tr.='<td>'.($item->val).'</td>';
                        $tr.='</tr>';
                    }
                    $tr.='</table>';
                    return  $tr;
                }
            ],[
                'format' => 'html',
                'attribute' => 'Final_indications_history',
                'value' => function($model){
                    $tr='<table class="table table-bordered">';
                    $tr.='<tr><th>Месяц</th><th>Значения</th></tr>';
                    foreach (json_decode($model->Final_indications_history) as $key=> $item){
                        $tr.='<tr>';
                        $tr.='<td>'.($item->dt).'</td>';
                        $tr.='<td>'.($item->val).'</td>';
                        $tr.='</tr>';
                    }
                    $tr.='</table>';
                    return  $tr;
                }
            ],
  //          'Final_indications_history:ntext',
    //        'Initial_indications_history:ntext',
        ],
    ]) ?>

</div>
