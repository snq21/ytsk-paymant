<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsIpuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-ipu-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'Client_id') ?>

    <?= $form->field($model, 'IPU_code') ?>

    <?= $form->field($model, 'Type_of_control') ?>

    <?= $form->field($model, 'Brand') ?>

    <?php // echo $form->field($model, 'Model') ?>

    <?php // echo $form->field($model, 'Serial_number') ?>

    <?php // echo $form->field($model, 'Installation_date') ?>

    <?php // echo $form->field($model, 'Date_of_verification') ?>

    <?php // echo $form->field($model, 'Installation_location') ?>

    <?php // echo $form->field($model, 'Initial_indications') ?>

    <?php // echo $form->field($model, 'Final_indications') ?>

    <?php // echo $form->field($model, 'Final_indications_history') ?>

    <?php // echo $form->field($model, 'Initial_indications_history') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
