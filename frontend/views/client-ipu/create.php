<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsIpu */

$this->title = Yii::t('app', 'Create Clients Ipu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clients Ipus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-ipu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
