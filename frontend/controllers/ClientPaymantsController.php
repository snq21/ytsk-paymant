<?php

namespace app\controllers;

use alhimik1986\PhpExcelTemplator\params\ExcelParam;
use alhimik1986\PhpExcelTemplator\PhpExcelTemplator;
use alhimik1986\PhpExcelTemplator\setters\CellSetterStringValue;
use app\models\Clients;
use Yii;
use app\models\ClientPaymants;
use app\models\ClientPaymantsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientPaymantsController implements the CRUD actions for ClientPaymants model.
 */
class ClientPaymantsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClientPaymants models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $searchModel = new ClientPaymantsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Displays a single ClientPaymants model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

        public function actionDocpay(){
            return $this->render('docPay');
        }

    public function actionView($id)
    {
        if (!Yii::$app->user->isGuest) {
            $fname=$this->Generate($id);
            return $this->render('view', [
                'model' => $this->findModel($id),
                'fname'=>$fname
            ]);
        }else{
            return $this->redirect(['site/login']);
        }

    }
    public  function Generate($id){
        $templateFile = './docs/template2.xlsx';

        $model=$this->findModel($id);
        $modelClient=Clients::findOne(['Id'=>$model->Client_id]);

        $fileName="/docs/".$model->Client_id."_".$id."_".date('m').".xls";
        if (!file_exists($fileName))
        {

            $params = [
                '[ed_izm]' => new ExcelParam(CellSetterStringValue::class, $model->Unit_of_measurement),
                '{month}' => new ExcelParam(CellSetterStringValue::class, $model->Calculation_period),
                '[count_people]' => new ExcelParam(CellSetterStringValue::class, $modelClient->Number_of_people),
                '{fio}' => new ExcelParam(CellSetterStringValue::class, $modelClient->Name),
                '{address}' => new ExcelParam(CellSetterStringValue::class, ($modelClient->City).",".($modelClient->Street).",".($modelClient->House).",".($modelClient->Building).",".($modelClient->Apartment)),
                '[ID]' => new ExcelParam(CellSetterStringValue::class, $model->Client_id),
                '[ELS]' => new ExcelParam(CellSetterStringValue::class, $modelClient->ELS),
                '[area]' => new ExcelParam(CellSetterStringValue::class, $modelClient->Area),
                '[area_home]' => new ExcelParam(CellSetterStringValue::class, $modelClient->House_area),
                '[area_mop]' => new ExcelParam(CellSetterStringValue::class, $modelClient->MOS_area),
                '[type_pay]' => new ExcelParam(CellSetterStringValue::class, $model->Type_of_payment),
                '[obem]' => new ExcelParam(CellSetterStringValue::class, $model->Quantity),
                '[tariff]' => new ExcelParam(CellSetterStringValue::class, $model->Tariff),
                '[nachislenno]' => new ExcelParam(CellSetterStringValue::class,$model->Accrued),
                '[nachisleno]' => new ExcelParam(CellSetterStringValue::class,$model->Accrued),
                '[pereraschet]' => new ExcelParam(CellSetterStringValue::class, $model->Allocation),
                '[ostat_nachalo]' => new ExcelParam(CellSetterStringValue::class, $model->Balance_at_the_beginning),
                '[payd]' => new ExcelParam(CellSetterStringValue::class, $model->Paid),
                '[normativ]' => new ExcelParam(CellSetterStringValue::class, $model->Normative),
                '[vid_nachisleniya]' => new ExcelParam(CellSetterStringValue::class, $model->Calculation_type),
                '[type_paymant]' => new ExcelParam(CellSetterStringValue::class, $model->Type_of_payment),
            ];
            PhpExcelTemplator::saveToFile($templateFile, ".".$fileName, $params);

        }

        return $fileName;
    }

    /**
     * Creates a new ClientPaymants model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->isGuest) {
            $model = new ClientPaymants();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->Id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }else{
            return $this->redirect(['site/login']);
        }



    }

    /**
     * Updates an existing ClientPaymants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->isGuest) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->Id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }else{
            return $this->redirect(['site/login']);
        }


    }

    /**
     * Deletes an existing ClientPaymants model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest) {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }else{
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Finds the ClientPaymants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientPaymants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientPaymants::findOne(['Id'=>$id,'Client_id'=>Yii::$app->user->getId()])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
