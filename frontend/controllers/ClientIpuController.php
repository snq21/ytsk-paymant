<?php

namespace app\controllers;

use Yii;
use app\models\ClientsIpu;
use app\models\ClientsIpuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientIpuController implements the CRUD actions for ClientsIpu model.
 */
class ClientIpuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClientsIpu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientsIpuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if (!Yii::$app->user->isGuest){
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            return $this->redirect(['site/login']);
        }

    }

    /**
     * Displays a single ClientsIpu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }else{
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Creates a new ClientsIpu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->isGuest) {
            $model = new ClientsIpu();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);


        }else{
            return $this->redirect(['site/login']);
        }

    }

    /**
     * Updates an existing ClientsIpu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        
        

        if (!Yii::$app->user->isGuest)
        {

            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()))
            {

                $found=false;
                if(strlen($model->Final_indications_history)>2)
                {
                    $objFinal=json_decode($model->Final_indications_history,true);
                    
                    foreach ($objFinal as $k1=>&$item){
                        if($item['dt']==date('m:Y'))
                        {
                            $item['val']=$model->Final_indications;
                            $found=true;
                        }
                    }    
                    $model->Final_indications_history=json_encode($objFinal);
                }
                
                if (!$found){
                    $objFinal[]=['dt'=>date('m:Y'),'val'=>$model->Final_indications];
                }
                $model->Final_indications_history=json_encode($objFinal);
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);

        }else{
            return $this->redirect(['site/login']);
        }

    }

    /**
     * Deletes an existing ClientsIpu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->isGuest) {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        }else{
            return $this->redirect(['site/login']);
        }


    }

    /**
     * Finds the ClientsIpu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientsIpu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientsIpu::findOne(['id'=>$id,'Client_id'=>Yii::$app->user->getId()])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'Не найден '));
    }
}
