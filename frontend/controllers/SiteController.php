<?php

namespace app\controllers;

use app\models\ClientPaymants;
use app\models\Clients;
use app\models\ClientsIpu;
use app\models\ForgotForm;
use app\models\Msg;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use alhimik1986\PhpExcelTemplator\PhpExcelTemplator;
use alhimik1986\PhpExcelTemplator\params\ExcelParam;
use alhimik1986\PhpExcelTemplator\setters\CellSetterStringValue;
use alhimik1986\PhpExcelTemplator\setters\CellSetterArrayValue;
use alhimik1986\PhpExcelTemplator\setters\CellSetterArray2DValue;




class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('index');
    }
    public function actionForgot()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ForgotForm();
        if (Yii::$app->request->isPost&& $model->load(Yii::$app->request->post()))
        {
            $client=$model->find_client();
            if (!empty($client)){
                $client->password=sha1($model->password);
                if ($client->save()){
                    Yii::$app->session->setFlash('forgoted');
                    return $this->refresh(); //$this->redirect(['site/login']);
                }
            }
            Yii::$app->session->setFlash('forgoted');
            //return $this->redirect(['site/login']);
        }

        $model->password = '';
        return $this->render('forgot',['model'=>$model]);
    }



    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionProfile()
    {
        if (!Yii::$app->user->isGuest){
            $modelClient=Clients::findOne(['Id'=>Yii::$app->user->identity->getId()]);
            $modelPaymant=ClientPaymants::find()->where(['Client_id'=>$modelClient->Id])
                ->andFilterWhere(['>', 'Tariff', 0])
            ->orderBy(['Type_of_payment' => SORT_DESC])->all();
            $modelIpu=ClientsIpu::find()->where(['Client_id'=>$modelClient->Id])->all();
            $modelMsg=Msg::findOne(['client_id'=>$modelClient->Id]);
            return $this->render('profile',['modelClient'=>$modelClient,'modelMsg'=>$modelMsg,
                'modelIpu'=>$modelIpu,'modelPayment'=>$modelPaymant
                ]);
        }else{
            return $this->redirect(['site/login']);
        }

    }
    public function actionSetting()
    {
        if (!Yii::$app->user->isGuest) {
            $modelClient = Clients::findOne(['Id' => Yii::$app->user->identity->getId()]);
            return $this->render('profile', ['modelCLient' => $modelClient]);
        }else{
            return $this->redirect(['site/login']);
        }
    }


}
