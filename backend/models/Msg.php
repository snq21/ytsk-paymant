<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "msg".
 *
 * @property int $id
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $message_client
 * @property string|null $message_admin
 * @property string|null $status
 * @property string $dt
 */
class Msg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'msg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'string'],
            [['dt'], 'safe'],
            [['email', 'phone', 'message_client', 'message_admin'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Почта',
            'phone' => 'Телефон',
            'message_client' => 'Сообщения от клиента',
            'message_admin' => 'Ответ Администратора',
            'status' => 'Статус',
            'dt' => 'Дата',
        ];
    }
}
