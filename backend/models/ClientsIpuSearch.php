<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientsIpu;

/**
 * ClientsIpuSearch represents the model behind the search form of `app\models\ClientsIpu`.
 */
class ClientsIpuSearch extends ClientsIpu
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'Client_id', 'IPU_code'], 'integer'],
            [['Type_of_control', 'Brand', 'Model', 'Serial_number', 'Installation_date', 'Date_of_verification', 'Installation_location', 'Final_indications_history', 'Initial_indications_history'], 'safe'],
            [['Initial_indications', 'Final_indications'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientsIpu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'Client_id' => $this->Client_id,
            'IPU_code' => $this->IPU_code,
            'Initial_indications' => $this->Initial_indications,
            'Final_indications' => $this->Final_indications,
        ]);

        $query->andFilterWhere(['like', 'Type_of_control', $this->Type_of_control])
            ->andFilterWhere(['like', 'Brand', $this->Brand])
            ->andFilterWhere(['like', 'Model', $this->Model])
            ->andFilterWhere(['like', 'Serial_number', $this->Serial_number])
            ->andFilterWhere(['like', 'Installation_date', $this->Installation_date])
            ->andFilterWhere(['like', 'Date_of_verification', $this->Date_of_verification])
            ->andFilterWhere(['like', 'Installation_location', $this->Installation_location])
            ->andFilterWhere(['like', 'Final_indications_history', $this->Final_indications_history])
            ->andFilterWhere(['like', 'Initial_indications_history', $this->Initial_indications_history]);

        return $dataProvider;
    }
}
