<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Clients".
 *
 * @property int $Id
 * @property string|null $Name
 * @property string|null $City
 * @property string|null $Street
 * @property string|null $House
 * @property string|null $Building
 * @property string|null $Apartment
 * @property string|null $Room
 * @property string|null $ELS
 * @property float|null $Area
 * @property float|null $House_area
 * @property float|null $MOS_area
 * @property int|null $Number_of_people
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id'], 'required'],
            [['Id', 'Number_of_people'], 'integer'],
            [['Area', 'House_area', 'MOS_area'], 'number'],
            [['Name', 'City', 'Street'], 'string', 'max' => 255],
            [['House', 'Building', 'Apartment', 'Room'], 'string', 'max' => 10],
            [['ELS'], 'string', 'max' => 200],
//            [['Id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ЛС',
            'Name' => 'ФИО',
            'City' => 'Город',
            'Street' => 'Улица',
            'House' => 'Дом',
            'Building' => 'Корпус',
            'Apartment' => 'Квартира',
            'Room' => 'Комната',
            'ELS' => 'ЕЛС',
            'Area' => 'Площадь',
            'House_area' => 'Площадь дома',
            'MOS_area' => 'Площадь МОП',
            'Number_of_people' => 'Количество человек',
        ];
    }
}
