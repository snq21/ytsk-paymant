<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clients;

/**
 * ClientsSearch represents the model behind the search form of `app\models\Clients`.
 */
class ClientsSearch extends Clients
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Id', 'Number_of_people'], 'integer'],
            [['Name', 'City', 'Street', 'House', 'Building', 'Apartment', 'Room', 'ELS'], 'safe'],
            [['Area', 'House_area', 'MOS_area'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clients::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Id' => $this->Id,
            'Area' => $this->Area,
            'House_area' => $this->House_area,
            'MOS_area' => $this->MOS_area,
            'Number_of_people' => $this->Number_of_people,
        ]);

        $query->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'City', $this->City])
            ->andFilterWhere(['like', 'Street', $this->Street])
            ->andFilterWhere(['like', 'House', $this->House])
            ->andFilterWhere(['like', 'Building', $this->Building])
            ->andFilterWhere(['like', 'Apartment', $this->Apartment])
            ->andFilterWhere(['like', 'Room', $this->Room])
            ->andFilterWhere(['like', 'ELS', $this->ELS]);

        return $dataProvider;
    }
}
