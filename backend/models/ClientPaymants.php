<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ClientPaymants".
 *
 * @property int $Id
 * @property int $Client_id
 * @property string|null $Calculation_period
 * @property string|null $Type_of_payment
 * @property string|null $Calculation_type
 * @property string|null $Unit_of_measurement
 * @property float|null $Tariff
 * @property float|null $Normative
 * @property float|null $Quantity
 * @property float|null $Accrued
 * @property float|null $Allocation
 * @property float|null $Paid
 * @property float|null $Balance_at_the_beginning
 */
class ClientPaymants extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientpaymants';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Client_id'], 'required'],
            [['Client_id'], 'integer'],
            [['Tariff', 'Normative', 'Quantity', 'Accrued', 'Allocation', 'Paid', 'Balance_at_the_beginning'], 'number'],
            [['Calculation_period', 'Type_of_payment', 'Calculation_type', 'Unit_of_measurement'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */


    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Client_id' => 'ЛС',
            'Calculation_period' => 'Период начисления',
            'Type_of_payment' => 'Вид платежа',
            'Calculation_type' => 'Вид начисления',
            'Unit_of_measurement' => 'Единица измерения',
            'Tariff' => 'Тариф',
            'Normative' => 'Норматив',
            'Quantity' => 'Количество',
            'Accrued' => 'Начислено',
            'Allocation' => 'Перерасчет',
            'Paid' => 'Оплачено',
            'Balance_at_the_beginning' => 'Остаток на начало',
        ];
    }
}
