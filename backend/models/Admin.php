<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "admin".
 *
 * @property int $id
 * @property string $fio
 * @property string|null $login
 * @property string|null $password
 * @property string|null $status
 * @property string $authKey
 * @property string $access_token
 */
class Admin extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'authKey', 'access_token'], 'required'],
            [['status'], 'string'],
            [['fio', 'password', 'authKey', 'access_token'], 'string', 'max' => 255],
            [['login'], 'string', 'max' => 20],
            [['login'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Fio',
            'login' => 'Login',
            'password' => 'Password',
            'status' => 'Status',
            'authKey' => 'Auth Key',
            'access_token' => 'Access Token',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->password = sha1($this->password);
            return true;
        } else {
            return false;
        }
    }


    /**
     * @inheritDoc
     */
    public static function findIdentity($id)
    {
        return Admin::findOne($id);
    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * @inheritDoc
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
    public function validatePassword($password){
        return $password===$this->password;
    }
}
