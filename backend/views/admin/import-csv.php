<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Импорт');
?>
<?php
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['method'=>'post','enctype' => 'multipart/form-data']]) ?>

<p>
<code>
Форматы файлов
IPU_месяц_год.csv--- ИПУ<br>
NA_месяц_год.csv--- Начисления<br>
</code>
</p>
<?= $form->field($model, 'csvFile')->fileInput(['class'=>'form-control btn btn-primary']) ?>

    <button class="btn btn-primary">Загрузить</button>

<?php ActiveForm::end() ?>