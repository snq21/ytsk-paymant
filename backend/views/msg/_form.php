<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Msg */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="msg-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message_client')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'message_admin')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'Прочитан' => 'Прочитан', 'Доставлен' => 'Доставлен', 'Обработан' => 'Обработан', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
