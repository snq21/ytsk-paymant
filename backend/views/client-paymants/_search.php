<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientPaymantsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-paymants-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'Client_id') ?>

    <?= $form->field($model, 'Calculation_period') ?>

    <?= $form->field($model, 'Type_of_payment') ?>

    <?= $form->field($model, 'Calculation_type') ?>

    <?php echo $form->field($model, 'Unit_of_measurement') ?>

    <?php echo $form->field($model, 'Tariff') ?>

    <?php echo $form->field($model, 'Normative') ?>

    <?php echo $form->field($model, 'Quantity') ?>

    <?php echo $form->field($model, 'Accrued') ?>

    <?php echo $form->field($model, 'Allocation') ?>

    <?php echo $form->field($model, 'Paid') ?>

    <?php echo $form->field($model, 'Balance_at_the_beginning') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Найти'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Сброс'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
