<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'Name') ?>

    <?= $form->field($model, 'City') ?>

    <?= $form->field($model, 'Street') ?>

    <?= $form->field($model, 'House') ?>

    <?php  echo $form->field($model, 'Building') ?>

    <?php  echo $form->field($model, 'Apartment') ?>

    <?php  echo $form->field($model, 'Room') ?>

    <?php  echo $form->field($model, 'ELS') ?>

    <?php  echo $form->field($model, 'Area') ?>

    <?php  echo $form->field($model, 'House_area') ?>

    <?php  echo $form->field($model, 'MOS_area') ?>

    <?php  echo $form->field($model, 'Number_of_people') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Поиск'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Сброс'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
