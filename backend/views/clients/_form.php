<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Id')->textInput() ?>

    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'City')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Street')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'House')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Building')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Apartment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Room')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ELS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Area')->textInput() ?>

    <?= $form->field($model, 'House_area')->textInput() ?>

    <?= $form->field($model, 'MOS_area')->textInput() ?>

    <?= $form->field($model, 'Number_of_people')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
