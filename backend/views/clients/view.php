<?php

use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $modelClient app\models\Clients */

$this->title = $modelClient->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Клиенты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clients-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Обновить'), ['update', 'id' => $modelClient->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $modelClient->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $modelClient,
        'attributes' => [
            'Id',
            'Name',
            'City',
            'Street',
            'House',
            'Building',
            'Apartment',
            'Room',
            'ELS',
            'Area',
            'House_area',
            'MOS_area',
            'Number_of_people',
        ],
    ]) ?>

</div>
<h2>Данные по платежам</h2>

    <?php Pjax::begin();

    ?>
    <?= GridView::widget([
        'dataProvider' => $modelPaymant,

        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            'Id',
            'Client_id' ,
            'Calculation_period' ,
            'Type_of_payment' ,
            'Calculation_type',
            'Unit_of_measurement' ,
            'Tariff',
            'Normative',
            'Quantity',
            'Accrued',
            'Allocation' ,
            'Paid' ,
            'Balance_at_the_beginning'
        ],
    ]); ?>
    <?php Pjax::end(); ?>


<h2>Данные ИПУ</h2>
<?php Pjax::begin(); ?>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $modelIpu,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'IPU_code',
        'Type_of_control',
        'Brand',
        'Model',
        'Serial_number',
        'Installation_date',
        'Date_of_verification',
        'Installation_location',
        'Initial_indications',
        'Final_indications',
        'Final_indications_history:ntext',
        'Initial_indications_history:ntext',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>

<?php Pjax::end(); ?>
