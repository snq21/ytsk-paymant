<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientsIpu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-ipu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Client_id')->textInput() ?>

    <?= $form->field($model, 'IPU_code')->textInput() ?>

    <?= $form->field($model, 'Type_of_control')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Brand')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Serial_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Installation_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Date_of_verification')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Installation_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Initial_indications')->textInput() ?>

    <?= $form->field($model, 'Final_indications')->textInput() ?>

    <?= $form->field($model, 'Final_indications_history')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Initial_indications_history')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
